/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author wili
 */
@Named
@RequestScoped
public class LoginBean extends PageBean{
    private String usuario = "padrao";
    private String senha = "padrão";
    private Boolean opadmin = false;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getOpadmin() {
        return opadmin;
    }

    public void setOpadmin(Boolean opadmin) {
        this.opadmin = opadmin;
    }
    
    public String Entrar(){
        String resp;
        if(usuario.contentEquals(senha))
            resp = "Usuario "+usuario+" logado"+ new Date();
        else
            resp = "Acesso Negado";
        return resp;
    }
    
}
